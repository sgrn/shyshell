# Reverse Shell

> Shyshell is an [asynchronous](http://en.wikipedia.org/wiki/Asynchrony_\(computer_programming\)) [reverse shell](http://resources.infosecinstitute.com/icmp-reverse-shell/) program. The repository supplies a client and server side. The server is capable of handling multiple client connections simultaneously.  
The server side is accessible through a [CLI](http://en.wikipedia.org/wiki/Command-line_interface), and provides a notification system.  

* Please, if you experience any errors, contact the author at <sangsoic@protonmail.com>. 
* Please, only execute this program on machines on which you're officially authorized to do so.


## Requirements

1. Install [python 3](http://www.python.org).

2. Install [git](http://git-scm.com).

3. Download [shyshell project](http://gitlab.com/scryphoxy/shyshell).

## Help

### UNIX-Like system

#### Client

`$ : git clone http://gitlab.com/scryphoxy/shyshell`

1. Open a shell.

2. Change current location to project source location after cloning the repository.

`.. $ : cd shyshell/client`

3. Edit 'main.py' allows clients to connect to the server IP address and port. If clients experience any connection trouble it will sequentially try other server IP address and ports.
Replace *[editer]* with the editor of your choice.

`../shyshell/client $ : [editer] main.py`

4. Change the following values.

```python
	servers_ip_address = ["127.0.0.1"]; # The client will sequentially try to connect to each server ip stored in this list
	servers_ports = [8765]; # Each port will be bind to each ip address of the same list index.  
```
5. Once changes are made, run 'main.py' on client side with root privileges (if possible)

`../shyshell/client # : python3 main.py`

* I highly recommend to compile client side script for [portability reasons](http://stackoverflow.com/questions/471191/why-compile-python-code).

#### Server

1. Change current location to the server source code directory.

`.. $ : cd shyshell/server`

2. Run 'main.py' with root privileges.

`../shyshell/server # : python3 main.py`

##### CLI Help

Command's id | Usage \(regex format\) | Description
-------------|--------------------|------------
help | \[\\s\]\*help\[\\s\]\* | print help menu
clear | \[\\s\]\*clear\[\\s\]\* | clear terminal screen
quit | \[\\s\]\*quit\[\\s\]\* | up the current working level, if current level is '/svr[0-9]+' root then server is closed 
listen | \[\\s\]\*listen\(\[\\s\]\+\[0\-9\]\*\)\*\[\\s\]\* | open and listen port for connection to happen
list | \[\\s\]\*list\(\[\\s\]\+\(\(port\)\|\(conn\)\|\(cmd\)\)\)?\[\\s\]\* | list connection\(s\), or commands, or listened port\(s\)
close | \[\\s\]\*close\(\[\\s\]\+\(\(port\[\\s\]\+\[0\-9\]\+\(,\[0\-9\]\+\)\*\)\|\(conn\[\\s\]\+\[a\-zA\-Z0\-9\.\\\-\_\]\+\(@\[a\-zA\-Z0\-9\.\\\-\_\]\+\)?\(:\[0\-9\]\+\)\(,\[a\-zA\-Z0\-9\.\\\-\_\]\+\(@\[a\-zA\-Z0\-9\.\\\-\_\]\+\)?\(:\[0\-9\]\+\)\)\*\)\)\)?\[\\s\]\* | close port and/or connection depending on precision
connect | \[\\s\]\*connect\(\[\\s\]\+\[a\-zA\-Z0\-9\.\\\-\_\]\+\(@\[a\-zA\-Z0\-9\.\\\-\_\]\+\)?\(:\[0\-9\]\+\):\(\(sys\)\|\(py\)\)\)?\[\\s\]\* | open interactive shell on a working connection with client 
set | \[\\s\]\*set\(\[\\s\]\+\(\(delay\)\|\(attempt\)\)=\[0\-9\]\+\)\*\[\\s\]\* | set server attributes

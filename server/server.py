#! /bin/python
# -*- coding:utf-8 -*-
#
# Copyright (C) 2019 shyshell author (Sangsoic)
# 
# This program is free software: you can redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

try:
	import os;
	import sys;
	import time;
	import socket;
	import threading;
	from queue import Queue;
	import collections;
	import re;
	from univ_rsrc import UnivRes
except Exception as ModuleError:
	print("[ERROR] : [MAIN] : [MSG:failed to import module] : [CAUSE:%s]" % (ModuleError));
	exit(1);



class Server(object):
	"""
	manages client connection
	"""
	ninstance = 0; # instance counter
	DELAY = 1; # delay for all instance by default
	MAX_ATTEMPT = 0; # max attempt for all instance by default
	ERROR = True;
	SUCCESS = False;
	SIG_TABLE = ["*fs*", "*ns*", "*kc*", "*kp*"];

	def __init__(self, *q_ports, delay=1, max_attempt=0, rvports=[]):
		REPPORT_TABLE = {
			"action" : "CONSTRUCT",
			"iid" : None,
			0 : [Server.SUCCESS, "SERVER successfully initialized"]
		};
		self._iid = Server.ninstance;
		Server.add_server();
		REPPORT_TABLE["iid"] = self._iid;
		self._delay = None;
		self.delay = delay;
		self._max_attempt = None;
		self.max_attempt = max_attempt;
		self._host = '192.168.0.100';
		# {port0: [socket0, {ip0: conn0, ip1: conn1, ..., ipn: connn}, [worker_status1, thread1]], ..., portn: [socketn, {ip0: conn0, ip1: client1, ..., ipn: connn}, [worker_statusn, threadn]]}
		self._server_data = {};
		# [qport0, qport1, ..., qportn]
		self._waiting_ports = collections.deque([]);
		self.add_qport(q_ports,rvports=rvports);
		self._prompt = [];
		Server.repport_back(REPPORT_TABLE,0);

	def _set_iid(self,value):
		"""

		Args:
		  value:

		Returns:
		"""
		REPPORT_TABLE = {"action" : "SET_IID","iid" : self._iid,0 : [Server.ERROR,"write access unauthorized"]};
		Server.repport_back(REPPORT_TABLE, 0);
		return;

	def _get_iid(self):
		""" """
		REPPORT_TABLE = {"action" : "GET_IID","iid" : self._iid,0 : [Server.SUCCESS,"read access authorized"]};
		return self._iid;

	iid = property(_get_iid,_set_iid);

	def _set_host(self, value):
		"""

		Args:
		  value:

		Returns:

		"""
		REPPORT_TABLE = {"action" : "SET_HOST","iid" : self._iid,0 : [Server.ERROR,"write access unauthorized"]};
		Server.repport_back(REPPORT_TABLE, 0);
		return;

	def _get_host(self):
		""" """
		REPPORT_TABLE = {"action" : "GET_HOST","iid" : self._iid,0 : [Server.ERROR,"read access unauthorized"]};
		Server.repport_back(REPPORT_TABLE, 0);
		return;
	host = property(_get_host, _set_host);

	def _set_server_data(self, value):
		"""

		Args:
		  value:

		Returns:

		"""
		REPPORT_TABLE = {"action" : "SET_server_data","iid" : self._iid,0 : [Server.ERROR,"write access unauthorized"]};
		Server.repport_back(REPPORT_TABLE, 0);
		return;

	def _get_server_data(self):
		""" """
		REPPORT_TABLE = {"action" : "GET_server_data","iid" : self._iid,0 : [Server.ERROR,"read access unauthorized"]};
		Server.repport_back(REPPORT_TABLE, 0);
		return;

	conn_gates = property(_get_server_data, _set_server_data);

	def _set_waiting_ports(self, value):
		"""

		Args:
		  value:

		Returns:

		"""
		REPPORT_TABLE = {"action" : "SET_WAITING_PORT","iid" : self._iid,0 : [Server.ERROR,"write access unauthorized"]};
		Server.repport_back(REPPORT_TABLE, 0);
		return;

	def _get_waiting_ports(self):
		""" """
		REPPORT_TABLE = {"action" : "GET_WAITING_PORT","iid" : self._iid,0 : [Server.ERROR,"read access unauthorized"]};
		Server.repport_back(REPPORT_TABLE, 0);
		return;
	waiting_ports = property(_get_waiting_ports, _set_waiting_ports);

	def _set_prompt(self, value):
		"""

		Args:
		  value:

		Returns:

		"""
		REPPORT_TABLE = {"action" : "SET_PROMPT","iid" : self._iid,0 : [Server.ERROR,"write access unauthorized"]};
		Server.repport_back(REPPORT_TABLE, 0);
		return;

	def _get_prompt(self):
		""" """
		REPPORT_TABLE = {"action" : "GET_PROMPT","iid" : self._iid,0 : [Server.ERROR,"read access unauthorized"]};
		Server.repport_back(REPPORT_TABLE, 0);
		return;
	prompt = property(_get_prompt, _set_prompt);

	def _set_delay(self,value):
		"""
		set _delay property
		Args:
		  value: value to test

		Returns:

		"""
		REPPORT_TABLE = {
			"action" : "SET_DELAY",
			"iid" : self._iid,
			0 : [Server.SUCCESS,"DELAY set TO %d"],
			1 : [Server.ERROR,"invalid DELAY"]
		}
		if isinstance(value, int):
			if value >= 0:
				self._delay = value;
				Server.repport_back(REPPORT_TABLE, 0, value);
				return;
		Server.repport_back(REPPORT_TABLE, 1);
		return;

	def _get_delay(self):
		""" """
		return _delay;

	delay = property(_get_delay, _set_delay);

	def _set_max_attempt(self,value):
		"""

		Args:
		  value:

		Returns:

		"""
		REPPORT_TABLE = {
			"action" : "SET_MAX_ATTEMPT",
			"iid" : self._iid,
			0 : [Server.SUCCESS,"MAX_ATTEMPT set TO %d"],
			1 : [Server.ERROR,"invalid MAX_ATTEMPT"]
		}
		if isinstance(value, int):
			if value >= 0:
				self._max_attempt = value;
				Server.repport_back(REPPORT_TABLE, 0, value);
				return;
		Server.repport_back(REPPORT_TABLE, 1);

	def _get_max_attempt(self):
		""" """
		return _max_attempt;

	max_attempt = property(_get_max_attempt, _set_max_attempt);

	def add_server(cls):
		"""
		increments the Server instance counter
		"""
		cls.ninstance += 1;
		return cls.ninstance;
	add_server = classmethod(add_server);

	def err_repport(cls, msg, siid, action, *param, cause=""):
		"""
		prints server activity on standard output
		Args:
		  msg: format string
		  siid: caller server instance id
		  action: caller method id
		  *param: variable elmt to concatenate into format
		  cause: exception error precision (Default value = "")

		Returns: final event related info string length

		"""
		oformat = "[SERVER_IID:{}] : [ERR] : [{}] : [MSG:{}]".format(siid,action,msg%param);
		if cause != "":
			oformat += " : [CAUSE:{}]".format(cause);
		oformat += "\n";
		sys.stderr.write(oformat);
		sys.stderr.flush();
		return len(oformat);
	err_repport = classmethod(err_repport);

	def out_repport(cls, msg, siid, action, *param, cause=""):
		"""
		prints server activity on standard error output
		Args:
		  msg: format string
		  siid: caller server instance id
		  action: caller method id
		  *param: variable elmt to concatenate into format
		  cause: exception error precision (Default value = "")

		Returns: final event related info string length

		"""
		oformat = "[SERVER_IID:{}] : [INF] : [{}] : [MSG:{}]".format(siid,action,msg%param);
		if cause != "":
			oformat += " : [CAUSE:{}]".format(cause);
		oformat += "\n";
		sys.stdout.write(oformat);
		sys.stdout.flush();
		return len(oformat);
	out_repport = classmethod(out_repport);

	def repport_back(cls, REPPORT_TABLE, index, *param, cause=None):
		"""
		chooses whether to print messages on stdout or stderr stream
		Args:
		  REPPORT_TABLE:
		  index: REPPORT_TABLE index of event corresponding format message
		  *param: variable elmt to concatenate into format
		  cause: exception error precision (Default value = None)

		Returns:

		"""
		if REPPORT_TABLE[index][0]:
			if cause != None:
				Server.err_repport(REPPORT_TABLE[index][1], REPPORT_TABLE["iid"], REPPORT_TABLE["action"], cause=cause, *param);
			else:
				Server.err_repport(REPPORT_TABLE[index][1], REPPORT_TABLE["iid"], REPPORT_TABLE["action"], *param);
		else:
			if cause != None:
				Server.out_repport(REPPORT_TABLE[index][1], REPPORT_TABLE["iid"], REPPORT_TABLE["action"], cause=cause, *param);
			else:
				Server.out_repport(REPPORT_TABLE[index][1], REPPORT_TABLE["iid"], REPPORT_TABLE["action"], *param);
		return;
	repport_back = classmethod(repport_back);

	def add_qport(self, *q_ports, rvports=[]):
		"""
		adds port within the queue
		Args:
		  *q_ports: tuples port(s) to push into queue
		  rvports: actual valid port(s) pushed into queue (Default value = [])

		Returns:

		"""
		REPPORT_TABLE = {
			"action": "ADD_WAITING_PORT",
			"iid" : self._iid,
			0 : [Server.SUCCESS,"no waiting port(s) to add into queue"],
			1 : [Server.SUCCESS,"%s waiting port(s) added into queue"]
		};
		to_add_ports = UnivRes.no_list_rec(q_ports,[]);
		to_add_ports = [port for port in to_add_ports if isinstance(port, int)];
		to_add_ports = list(set(to_add_ports));
		to_add_ports = [port for port in to_add_ports if port >= 0 and port <= 65535];
		if not list(to_add_ports):
			Server.repport_back(REPPORT_TABLE,0);
			return;
		self._waiting_ports.extend(to_add_ports);
		rvports.extend(to_add_ports);
		Server.repport_back(REPPORT_TABLE, 1, to_add_ports);
		return;

	def list_qport(self):
		"""
		lists port within the queue
		Returns: currently waiting port(s) list
		"""
		REPPORT_TABLE = {
			"action": "LIST_WAITING_PORT",
			"iid" : self._iid,
			0 : [Server.SUCCESS,"no port waiting into queue"],
			1 : [Server.SUCCESS,"%s port(s) waiting into queue"]
		};
		l_ports = list(self._waiting_ports);
		if not list(l_ports): Server.repport_back(REPPORT_TABLE,0);
		else: Server.repport_back(REPPORT_TABLE,1,l_ports);
		return l_ports;

	def del_qport(self, start=0, end=0):
		"""
		removes port within the queue
		Args:
		  start: where to start deleting into queue ? (Default value = 0)
		  end: where to stop deleting into queue ? (Default value = 0)

		Returns: list of deleted port(s)

		"""
		REPPORT_TABLE = {
			"action": "DEL_WAITING_PORT",
			"iid" : self._iid,
			0 : [Server.SUCCESS,"no waiting port into queue"],
			1 : [Server.SUCCESS,"%s port(s) removed from queue"]
		};
		if not list(self._waiting_ports):
			Server.repport_back(REPPORT_TABLE,0);
			return [];
		new = list(self._waiting_ports)
		target = new[start:end].copy();
		del new[start:end];
		self._waiting_ports = collections.deque(new);
		Server.repport_back(REPPORT_TABLE,1,list(target));
		return target;

	def list_oport(self):
		"""
		lists opened server port
		Returns: currently opened port(s) list
		"""
		REPPORT_TABLE = {
			"action": "LIST_OPEN_PORT",
			"iid" : self._iid,
			0 : [Server.SUCCESS,"no opened port into list"],
			1 : [Server.SUCCESS,"%s opened port(s) list"]
		};
		l_port = list(self._server_data.keys());
		if not l_port: 
                    Server.repport_back(REPPORT_TABLE,0);
		else: 
                    Server.repport_back(REPPORT_TABLE,1,l_port);
		return l_port;

	def list_conn(self, *ports):
		"""
		lists active connection
		Args:
		  *ports: port(s) to list connection on specific port

		Returns: connection(s) list

		"""
		REPPORT_TABLE = {
			"action": "LIST_CONN",
			"iid" : self._iid,
			0 : [Server.SUCCESS,"no given valid port(s) to list connection(s) on"],
			1 : [Server.SUCCESS,"no connection established on port(s) %s"],
			2 : [Server.SUCCESS,"%s connection(s) on port(s) %s"]
		};

		ports = UnivRes.no_list_rec(ports, []);
		ports = list(set(ports));
		valid_ports = [port for port in ports if port in self.list_oport()];
		invalid_ports = set(ports) - set(valid_ports);
		connections = [];

		if not valid_ports:
			Server.repport_back(REPPORT_TABLE,0);
			return connections;

		for port in valid_ports:
			last_len = len(connections);
			for conn in list(self._server_data[port][1].keys()):
				connections.append(conn);
			if len(connections) == last_len:
				Server.repport_back(REPPORT_TABLE,1,port);
			else:
				Server.repport_back(REPPORT_TABLE,2,connections[last_len:len(connections)],port);

		if not connections:
			Server.repport_back(REPPORT_TABLE,1,valid_ports);
		else:
			Server.repport_back(REPPORT_TABLE,2,connections,valid_ports);
		return connections;

	def list_portwconn(self, *connections):
		"""
		lists port with active connection
		Args:
		  *connections: targeted connection(s) to list

		Returns: port(s) list

		"""
		REPPORT_TABLE = {
				"action" : "LIST_PORTWCONN",
				"iid" : self._iid,
				0 : [Server.SUCCESS, "%s port(s) with active connection"]
		};
		connections = UnivRes.no_list_rec(connections,[]);
		valid_conns = list(set(connections));
		t_ports = [port for conn in valid_conns for port in self.list_oport() if conn in self.list_conn(port)];
		Server.repport_back(REPPORT_TABLE, 0, t_ports);
		return t_ports;

	def list_listened_port(self):
		"""
		lists port currently listened (where a daemonic thread is currently handling connections on it|them)
		Returns: port(s) list
		"""
		REPPORT_TABLE = {
				"action" : "LIST_LISTENED_PORT",
				"iid" : self._iid,
				0 : [Server.SUCCESS, "%s currently in listening"]
		};
		t_ports = [port for port in self.list_oport() if self._server_data[port][2][0]];
		Server.repport_back(REPPORT_TABLE, 0, t_ports);
		return t_ports;

	def create_socket(self, *q_ports, rvports=[]):
		"""
		creates socket on first port entered in queue
		Args:
		  *q_ports: port(s) to add into waiting queue (can be empty if waiting queue already contains port(s))
		  rvports: actual valid port(s) pushed into queue (Default value = [])

		Returns: boolean execution status code

		"""
		REPPORT_TABLE = {
			"action": "CREATE_SOCKET",
			"iid" : self._iid,
			0 : [Server.ERROR,"no waiting port to create socket on"],
			1 : [Server.ERROR,"fail to create socket on port %d"],
			2 : [Server.SUCCESS,"socket successfully created on port %d"]
		};
		exit_status = False;
		self.add_qport(*q_ports, rvports=rvports);
		if not list(self._waiting_ports):
			Server.repport_back(REPPORT_TABLE,0);
			return exit_status;
		while len(self._waiting_ports) != 0:
			port = self._waiting_ports.popleft();
			try:
				server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
			except Exception as SocketCreatingError:
				Server.repport_back(REPPORT_TABLE,1,port,cause=SocketCreatingError);
				exit_status = True;
				self.close(exit_status);
			self._server_data[port] = [server_socket, {}, [False, None]];
			Server.repport_back(REPPORT_TABLE,2,port);
		return exit_status;

	def bind_socket(self, *ports):
		"""
		binds port to socket
		Args:
		  *ports: port(s) list to be binded

		Returns: boolean execution status code

		"""
		REPPORT_TABLE = {
			"action": "BIND_SOCKET",
			"iid" : self._iid,
			0 : [Server.ERROR,"fail to bind socket with port %d retry %d/%d in %d sec.."],
			1 : [Server.ERROR,"no valid port to bind with socket"],
			2 : [Server.SUCCESS,"socket successfully binded with port %d"]
		};
		exit_status = False;
		def bind_one_socket(port, curr_attempt):
			"""
			tries to bind a port to a socket
			Args:
			  port: target port
			  curr_attempt: current attempt counter

			Returns:

			"""
			try:
				curr_attempt += 1;
				self._server_data[port][0].bind((self._host,port));
				self._server_data[port][0].listen(5);
			except Exception as SocketBindingError:
				Server.repport_back(REPPORT_TABLE,0,port,curr_attempt,self._max_attempt,self._delay,cause=SocketBindingError);
				if curr_attempt == self._max_attempt:
					raise Exception(SocketBindingError);
				time.sleep(self._delay);
				bind_one_socket(port, curr_attempt);
			return;

		ports = UnivRes.no_list_rec(ports,[]);
		ports = list(set(ports));
		valid_ports = [port for port in ports if port in self.list_oport()];
		invalid_ports = set(ports) - set(valid_ports);
		curr_attempt = 0;

		if not list(valid_ports):
			Server.repport_back(REPPORT_TABLE,1);
			return exit_status;
		for port in valid_ports:
			try:
				bind_one_socket(port, curr_attempt);
			except Exception as SocketBindingError:
				exit_status = True;
				continue;
			Server.repport_back(REPPORT_TABLE,2,port);
		return exit_status;

	def start_listening(self,*ports):
		"""
		starts a daemonic thread to handle connection on specific port(s)
		Args:
		  *ports: targeted port(s) list

		Returns: boolean execution status code

		"""
		REPPORT_TABLE = {
			"action" : "START_LISTENING",
			"iid" : self._iid,
			0 : [Server.ERROR, "'%s' daemonic thread failed to accept connection on %d port"],
			1 : [Server.SUCCESS, "'%s' daemonic thread successfully started on %d port"]
		};
		exit_status = False;
		def worker(port, worker_status):
			"""
			wrapper worker lauched by a daemonic thread
			Args:
			  port: target port
			  worker_status: activity worker status (if true: worker is running else: worker is killed by server control structure)

			Returns:

			"""
			def accept_conn_on_port(port):
				"""
				accepts connection a specific port
				Args:
				  port: target port

				Returns: boolean execution status code

				"""
				lhost, gaddr, cid = "", "", "";
				#print("\n<<<TRY TO ACCEPT CONNECTION ...>>>\n");
				conn, gaddr = self._server_data[port][0].accept();
				gaddr = gaddr[0];
				#print("\n<<<CONNECTION '%s'ADDR accepted>>>\n" % (gaddr));
				conn.setblocking(1);
				lhost = conn.recv(1024)
				lhost = lhost.decode("utf-8");
				if not lhost:
					#print("\n<<<UNABLE TO IDENTIFY CLIENT>>>\n");
					return;
				#print("\n<<<HOST RECEIVED '%s'>>>\n" % (lhost));
				cid = "%s@%s" % (gaddr, lhost);
				#print("\n<<<CLIENT cid IS '%s'>>>\n" % (cid));
				self._server_data[port][1][cid] = conn;
				print("\n<<<NEW CLIENT CONNECTION '%s' on port '%s'>>>\n" % (cid, port));
				return;

			if self.close_conn(port,*self.list_conn(port)):
				exit_status = True;
				return exit_status;
			while worker_status[0]:
				try:
					accept_conn_on_port(port);
				except Exception as SocketAcceptConnError:
					Server.repport_back(REPPORT_TABLE, 0, threading.current_thread(), port, cause=SocketAcceptConnError);
			return;
		ports = UnivRes.no_list_rec(ports,[]);
		ports = list(set(ports));
		valid_ports = [port for port in ports if port in self.list_oport()];
		invalid_ports = set(ports) - set(valid_ports);

		for port in valid_ports:
			thread = self._server_data[port][2][1] = threading.Thread(target=worker, args=(port,self._server_data[port][2]));
			thread.daemon = True;
			thread.start();
			self._server_data[port][2][0] = True;
			time.sleep(1);
			Server.repport_back(REPPORT_TABLE, 1, thread, port);
		return exit_status;

	def introduce(self, cid, port, shelltype="sys"):
		"""
		introduces interactive communication with client (socket level routine)
		Args:
		  cid: client id
		  shelltype: shell type precision (sys|py) (Default value = "sys")
		  port: port where connection is handled

		Returns: boolean execution status code, and current working directory received from client

		"""
		REPPORT_TABLE = {
			"action" : "GET_CWD",
			"iid" : self._iid,
			0 : [Server.ERROR,"no connection founded for '%s' cid on port %s"],
			1 : [Server.ERROR,"failed to send data to %s 'cid' on port %d"],
			2 : [Server.ERROR,"failed to received data from '%s' cid on port %d"],
			3 : [Server.SUCCESS,"data successfully sended to '%s' cid on port %d"],
			4 : [Server.SUCCESS,"cwd successfully received from '%s' cid on port %d"],
			5 : [Server.ERROR,"port %s is not opened"]
		};
		exit_status = False;
		conn, cwd = None, None;

		if not port in self.list_oport():
			exit_status = True;
			Server.repport_back(REPPORT_TABLE, 5, port);
			return exit_status, cwd;


		if cid in self.list_conn(port):
			conn = self._server_data[port][1][cid];
		else:
			Server.repport_back(REPPORT_TABLE, 0, cid, port);
			exit_status = True;
			return exit_status, cwd;

		try:
			conn.send(str.encode(shelltype));
		except Exception as SocketSendError:
			exit_status = True;
			Server.repport_back(REPPORT_TABLE, 1, cid, port, cause=SocketSendError);
			return exit_status, cwd;

		Server.repport_back(REPPORT_TABLE, 3, cid, port);

		try:
			cwd = conn.recv(2097152).decode("utf-8");
		except Exception as SocketRecvError:
			exit_status, cwd = True, None;
			Server.repport_back(REPPORT_TABLE, 2, cid, port, cause=SocketRecvError);
			return exit_status, cwd;

		Server.repport_back(REPPORT_TABLE,4, cid, port);

		return exit_status, cwd;

	def recv_output(self, cid, instruct, port):
		"""
		sends command and receives output from target client (socket level routine)
		Args:
		  cid: client id
		  instruct: instructions to send
		  port: port where connection is handled

		Returns: boolean execution status code, and the output from client

		"""

		REPPORT_TABLE = {
			"action" : "GET_OUPUT",
			"iid" : self._iid,
			0 : [Server.ERROR,"no connection founded for '%s' cid on port %s"],
			1 : [Server.ERROR,"failed to send data to %s 'cid' on port %d"],
			2 : [Server.ERROR,"failed to received data from '%s' cid on port %d"],
			3 : [Server.SUCCESS,"data successfully sended to '%s' cid on port %d"],
			4 : [Server.SUCCESS,"output successfully received from '%s' cid on port %d"],
			5 : [Server.ERROR,"no data to send to '%s' cid"],
			6 : [Server.ERROR,"port %s is not opened"]
		};

		exit_status = False;
		conn, output = None, None;

		if not instruct:
			exit_status = True;
			Server.repport_back(REPPORT_TABLE,5, cid);
			return exit_status, output;

		data = str.encode(instruct);

		if not port in self.list_oport():
			exit_status = True;
			Server.repport_back(REPPORT_TABLE,6, port);
			return exit_status, output;


		if cid in self.list_conn(port):
			conn = self._server_data[port][1][cid];
		else:
			Server.repport_back(REPPORT_TABLE, 0, cid, port);
			exit_status = True;
			return exit_status, output;

		try:
			conn.send(data);
		except Exception as SocketSendError:
			exit_status = True;
			Server.repport_back(REPPORT_TABLE, 1, cid, port, cause=SocketSendError);
			return exit_status, output;

		Server.repport_back(REPPORT_TABLE, 3, cid, port);

		try:
			conn.send(str.encode(" "));
			output = conn.recv(2097152).decode("utf-8");
		except Exception as SocketRecvError:
			exit_status, output = True, None;
			Server.repport_back(REPPORT_TABLE, 2, cid, port, cause=SocketRecvError);
			return exit_status, output;

		Server.repport_back(REPPORT_TABLE,4, cid, port);

		return exit_status, output;

	def stop_listening(self, *ports):
		"""
		stops daemonic thread currently handling connection on specific port
		Args:
		  *ports: targeted port(s) list

		Returns:

		"""
		REPPORT_TABLE = {
				"action" : "STOP_LISTENING",
				"iid" : self._iid,
				0 : [Server.SUCCESS, "try to stop listening %d port"],
				1 : [Server.SUCCESS, "listining process stopped %d"],
				2 : [Server.ERROR, "failed during the ender teller socket process"]
		};

		ports = UnivRes.no_list_rec(ports,[]);
		ports = list(set(ports));
		valid_ports = [port for port in ports if port in self.list_oport()];
		invalid_ports = set(ports) - set(valid_ports);
		t_ports = [port for port in self.list_listened_port() if port in valid_ports];

		for port in t_ports:
			self._server_data[port][2][0] = False;
			try:
				tmp_s = socket.socket();
				tmp_s.connect((self._host,port));
				tmp_s.shutdown(2);
				tmp_s.close();
			except Exception as SocketError:
				Server.repport_back(REPPORT_TABLE, 2, cause=SocketError);
			Server.repport_back(REPPORT_TABLE, 0, port);
			self._server_data[port][2][1].join();
			Server.repport_back(REPPORT_TABLE, 1, port);
		return;

	def close_conn(self, port, *address, signal=None):
		"""
		closes connection a specific port
		Args:
		  port: targeted port
		  *address: client(s) id list
		  signal: signal sent to client before ending connection

		Returns: boolean execution status code

		"""
		REPPORT_TABLE = {
			"action": "CLOSE_CONN",
			"iid" : self._iid,
			0 : [Server.ERROR,"fail to close %s connection on port %d retry %d/%d in %d sec.."],
			1 : [Server.SUCCESS,"port %d is not open"],
			2 : [Server.SUCCESS,"no connection established on port %d"],
			3 : [Server.SUCCESS,"connection %s on port %d successfully closed"],
			4 : [Server.SUCCESS,"ending connection signal '%s' successfully sent"],
			5 : [Server.ERROR,"fail to send ending connection signal '%s'"]
		};
		exit_status = False;
		def close_one_conn(port, address, signal, curr_attempt):
			"""
			tries to close a connection on a specific port
			Args:
			  port: targeted port
			  address: client id
			  curr_attempt: current attempt counter

			Returns:

			"""
			if curr_attempt == 0:
				try:
					self._server_data[port][1][address].send(str.encode(signal));
				except Exception as SocketSendError:
					Server.repport_back(REPPORT_TABLE,5,signal, cause=SocketSendError);
				else:
					Server.repport_back(REPPORT_TABLE,4,signal);
			try:
				curr_attempt += 1;
				self._server_data[port][1][address].shutdown(2);
				self._server_data[port][1][address].close();
			except Exception as SocketConnClosingError:
				Server.repport_back(REPPORT_TABLE,0,address,port,curr_attempt,self._max_attempt,self._delay,cause=SocketConnClosingError);
				if curr_attempt == self._max_attempt:
					raise Exception(SocketConnClosingError);
				time.sleep(self._delay);
				close_one_conn(port, address, curr_attempt);
			if curr_attempt == 1:
				del self._server_data[port][1][address];
			return;

		if not signal:
			signal = Server.SIG_TABLE[3];

		if not port in self.list_oport():
			Server.repport_back(REPPORT_TABLE,1,port)
			return exit_status;
		address = UnivRes.no_list_rec(address, []);
		address = list(set(address));
		valid_address = [addr for addr in address if addr in self.list_conn(port)];
		invalid_address = set(address) - set(valid_address);
		curr_attempt = 0;

		if not list(valid_address):
			Server.repport_back(REPPORT_TABLE,2,port);
			return exit_status;
		for addr in valid_address:
			try:
				close_one_conn(port, addr, signal, curr_attempt);
			except Exception as SocketConnClosingError:
				exit_status = True;
				continue;
			Server.repport_back(REPPORT_TABLE,3,addr,port);
		return exit_status;

	def close_socket(self, *ports):
		"""
		closes socket on specific port (it will close recursively all connection(s) and all daemonic thread(s) opened on the port)
		Args:
		  *ports: targeted port(s) list

		Returns: boolean execution status code

		"""
		REPPORT_TABLE = {
			"action": "CLOSE_SOCKET",
			"iid" : self._iid,
			0 : [Server.ERROR,"fail to close socket on port %d retry %d/%d in %d sec.."],
			1 : [Server.SUCCESS,"no socket to close"],
			2 : [Server.SUCCESS,"socket on port %d successfully closed"]
		};
		exit_status = False;
		def close_one_socket(port, curr_attempt):
			"""
			try to close one socket
			Args:
			  port: targeted port
			  curr_attempt: current attempt counter

			Returns: boolean execution status code

			"""
			exit_status = False;
			try:
				curr_attempt += 1;
				self._server_data[port][0].close();
			except Exception as SocketClosingError:
				Server.repport_back(REPPORT_TABLE,0,port,curr_attempt,self._max_attempt,self._delay, cause=SocketClosingError);
				if curr_attempt != self._max_attempt:
					time.sleep(self._delay);
					close_one_socket(port, curr_attempt);
				exit_status = True;
			if curr_attempt == 1:
				if self.close_conn(port, *self.list_conn(port)):
					exit_status = True;
				else: del self._server_data[port][:];
				if not exit_status:
					del self._server_data[port];
			return exit_status;

		ports = UnivRes.no_list_rec(ports, []);
		ports = list(set(ports));
		valid_ports = [port for port in ports if port in self.list_oport()];
		invalid_ports = set(ports) - set(valid_ports);
		curr_attempt = 0;

		if not list(valid_ports):
			Server.repport_back(REPPORT_TABLE,1);
			return exit_status;
		self.stop_listening(*valid_ports);
		for port in valid_ports:
			if close_one_socket(port, curr_attempt):
				exit_status = True;
				continue;
			Server.repport_back(REPPORT_TABLE,2,port)
		return exit_status;


	def close(self, caller_exit_status=False):
		"""
		closes the whole server instance
		Args:
		  caller_exit_status: execution status (Default value = False)

		Returns: close server instance boolean execution status code

		"""
		REPPORT_TABLE = {
			"action": "CLOSE_SERVER",
			"iid" : self._iid,
			0 : [Server.ERROR,"closing server with error"],
			1 : [Server.SUCCESS,"closing server with success"],
		};
		exit_status = False;
		self.del_qport(0,len(self._waiting_ports));
		if self.close_socket(*self.list_oport()): exit_status = True;
		if exit_status: Server.repport_back(REPPORT_TABLE,0);
		else: Server.repport_back(REPPORT_TABLE,1);
		return exit_status;

	def cli_run(self):
		"""
		runs server instance in command line interface mode
		Returns: boolean execution status code
		"""
		def interactive_shell(cid, port, shelltype="sys"):
			"""
			starts system or python interactive shell  with client
			Args:
			  cid: client id
			  shelltype:  shell type precision (system or python) (Default value = "sys")
			  port: port where connection is handled (if none find first port with cid connection on it) (Default value = None)

			Returns: boolean execution status code

			"""
			exit_status = False;
			cwd, output = None, None;
			self._prompt.append("/shell.%s/%s" % (shelltype, cid));
			while not exit_status:
				exit_status, cwd = self.introduce(cid, port, shelltype=shelltype);
				if exit_status:
					if self.close_conn(port, cid, signal=cprpt_return):
						exit_status = True;
					break;
				while True:
					try:
						cprpt_return = input("\nSERVER:[%s]\nCLIENT:[%s]> " % (''.join(self._prompt), cwd));
						if re.fullmatch(r"[\s]*",cprpt_return):
							continue
						break;
					except KeyboardInterrupt:
						print("\n*[INF] : [MSG: keyboard signal interrupt the current task]");
						continue;
					except EOFError:
						cprpt_return = Server.SIG_TABLE[0];
						break;
				print();

				if re.fullmatch(r"[\s]*([*]%s[*])[\s]*"%(Server.SIG_TABLE[2]),cprpt_return) or re.fullmatch(r"[\s]*([*]%s[*])[\s]*"%(Server.SIG_TABLE[3]),cprpt_return):
					print("*[INF] [MSG: connection with '%s' has been killed (session erases)]" % (cid));
					if self.close_conn(port, cid, signal=cprpt_return):
						exit_status = True;
					break;
				exit_status, output = self.recv_output(cid, cprpt_return, port);
				if exit_status:
					if self.close_conn(port, cid, signal=cprpt_return):
						exit_status = True;
					print("*[ERR] [MSG: receiving output from client with error status code]");
					break;
				else:
					print("*[INF] [MSG: receiving output from client with success status code]");
				if re.fullmatch(r"[\s]*([*]%s[*])[\s]*"%(Server.SIG_TABLE[0]),cprpt_return):
					print("*[INF] [MSG: interactive shell with '%s' has been frozen (session waiting)]" % (cid));
					break;
				elif re.fullmatch(r"[\s]*([*]%s[*])[\s]*"%(Server.SIG_TABLE[1]),cprpt_return):
					print("*[INF] [MSG: interactive shell with '%s' has been killed (session erased)]" % (cid));
					break;
				else:
					print("\n*[OUTPUT FROM CLIENT EXECUTION]:");
					print(output);
					print("____\n");
			del self._prompt[-1];
			if exit_status:
				print("*[ERR] [MSG: exiting interactive connection with '%s' client]" % (cid));
			else:
				print("*[INF] [MSG: exiting interactive connection with '%s' client]" % (cid));
			return exit_status;

		def print_listened_ports():
			"""
			prints out an enumeration menu gathering current listened ports(s)
			"""
			print("*[PORT_MENU]:");
			all_listened_ports = self.list_listened_port();
			for port in all_listened_ports:
				print("*[PORT:%d]" % (port));
			print("____");

		def print_connection():
			"""
			prints out an enumeration menu gathering current client(s) connection(s)
			"""
			print("*[CONNECTION_MENU]:");
			all_conn = self.list_conn(*self.list_oport());
			for a, port in enumerate(self.list_oport()):
				for b, items in enumerate(self._server_data[port][1].items()):
					print("*[STATUS:ONLINE] [CID:%s] [PORT:%d] [ADDR:%s]" % ("%d%d"%(a,b),port,items[0]));
			print("____");

		def print_help():
			"""
			prints out an enumeration menu gathering all cli commands and their descriptions
			"""
			HELP = (
				"[CMD:help] [DESC: print this menu]",
				"[CMD:quit] [DESC: up the current working level, if current level is '%s' root then server is closed]" % (self._prompt[0]),
				"[CMD:listen] [DESC: open and listen port for connection to happen]",
				"[CMD:list] [DESC: list connection(s), or commands, or listened port(s)]",
				"[CMD:close] [DESC: close port | connection depending on precision]",
				"[CMD:connect] [DESC: open interactive shell on a working connection with client]",
				"[CMD:clear] [DESC: clear the terminal screen]",
				"[CMD:set] [DESC: set server attributes]"
			);
			print("*[HELP_MENU]:");
			for cmd in HELP: print("*%s"%cmd);
			print("____");

		exit_status = False;
		self._prompt.append("/svr%d" % (self._iid));
		if os.sep == "\\": clear = lambda: os.system("cls");
		else: clear = lambda: os.system("clear");
		print("*[RUNNING SERVER %d IN CLI MODE]" % (self._iid));
		while True:
			try:
				cprpt_return = input(''.join(self._prompt) + '> ');
				if re.fullmatch(r"[\s]*",cprpt_return):
					continue;
				if re.fullmatch(r"[\s]*quit[\s]*", cprpt_return):
					break;
				elif re.fullmatch(r"[\s]*listen([\s]+[0-9]*)*[\s]*", cprpt_return):
					ports = re.findall(r"[0-9]+",cprpt_return);
					ports = [int(port) for port in ports];
					if ports:
						if self.create_socket(*ports) or self.bind_socket(self.list_oport()) or self.start_listening(self.list_oport()):
							exit_status = True;
							break;
					continue;
				elif re.fullmatch(r"[\s]*list([\s]+((port)|(conn)|(cmd)))?[\s]*", cprpt_return):
					tobe_list = re.findall(r"(port)|(conn)|(cmd)",cprpt_return);
					if tobe_list:
						if tobe_list[0][0] == "port":
							print_listened_ports();
						elif tobe_list[0][1] == "conn":
							print_connection();
						elif tobe_list[0][2] == "cmd":
							print_help();
					else:
						print_listened_ports();
						print_connection();
						print_help();
					continue;
				elif re.fullmatch(r"[\s]*close([\s]+((port[\s]+[0-9]+(,[0-9]+)*)|(conn[\s]+[a-zA-Z0-9.\-_]+(@[a-zA-Z0-9.\-_]+)?(:[0-9]+)(,[a-zA-Z0-9.\-_]+(@[a-zA-Z0-9.\-_]+)?(:[0-9]+))*)))?[\s]*", cprpt_return):
					if re.search(r"port", cprpt_return):
						tobe_close = re.search(r"([0-9]+,?)+",cprpt_return);
						tobe_close_listed = cprpt_return[tobe_close.start():tobe_close.end()].split(',');
						tobe_close_listed = [int(port) for port in tobe_close_listed];
						if self.close_socket(*tobe_close_listed):
							exit_status = True;
							break;
					elif re.search(r"conn", cprpt_return):
						remove = re.search(r"[\s]*close[\s]*conn", cprpt_return);
						cprpt_return = cprpt_return[:remove.start()] + cprpt_return[remove.end():]
						tobe_close = re.search(r"([a-zA-Z0-9.\-_]+(@[a-zA-Z0-9.\-_]+)?(:[0-9]+),?)+",cprpt_return);
						tobe_close_listed = cprpt_return[tobe_close.start():tobe_close.end()].split(',');
						ports = list(set([int(conn.split(':')[1]) for conn in tobe_close_listed]));
						for port in ports:
							if self.close_conn(port, *[conn.split(':')[0] for conn in tobe_close_listed if int(conn.split(':')[1]) == port]):
								exit_status = True;
								break;
						if exit_status:
							break;
					continue;
				elif re.fullmatch(r"[\s]*connect([\s]+[a-zA-Z0-9.\-_]+(@[a-zA-Z0-9.\-_]+)?(:[0-9]+):((sys)|(py)))?[\s]*", cprpt_return):
					splited_cprpt_return = re.split(r"[\s]+",cprpt_return);
					splited_cprpt_return = [elmt for elmt in splited_cprpt_return if elmt];
					if len(splited_cprpt_return) == 2:
						connection_info = splited_cprpt_return[1].split(":");
						addr, port, type = connection_info;
						port = int(port);
						interactive_shell(addr,port,type);
					continue;
				elif re.fullmatch(r"[\s]*help[\s]*",cprpt_return):
					print_help();
					continue;
				elif re.fullmatch(r"[\s]*clear[\s]*",cprpt_return):
					clear();
					continue;
				elif re.fullmatch(r"[\s]*set([\s]+((delay)|(attempt))=[0-9]+)*[\s]*",cprpt_return):
					delay_attr = re.findall(r"delay=[0-9]+",cprpt_return);
					attempt_attr = re.findall(r"attempt=[0-9]+",cprpt_return);
					if delay_attr:
						self.delay = int(delay_attr[-1].split('=')[1]);
					if attempt_attr:
						self.max_attempt = int(attempt_attr[-1].split('=')[1]);
					continue;
			except KeyboardInterrupt:
				print("\n*[INF] : [MSG: keyboard signal interrupt the current task]");
				continue;
			except EOFError:
				print();
				break;
			print("*[ERR] : [MSG: '%s' bad syntax detected]" % (cprpt_return));
			print("*[INF] : [MSG: try 'help' command]");
		del self._prompt[-1];
		print("____");
		if self.close(exit_status):
			exit_status = True;
		return exit_status;

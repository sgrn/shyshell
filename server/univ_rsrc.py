#! /bin/python3
# -*- coding:utf-8 -*-
#
# Copyright (C) 2019 shyshell author (Sangsoic)
# 
# This program is free software: you can redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

try:
	import signal;
	import socket;
	import collections;
except Exception as ModuleError:
	print("[ERROR] : [MAIN] : [MSG:failed to import module] : [CAUSE:%s]" % (ModuleError));
	exit(1);

class UnivRes:
	"""
	contains non specific routines needed among the server program
	"""
	LOCALHOST = socket.gethostname();

	def signal_handler(**signals):
		"""
		binds numeric signals and routines togather
		Args:
		  **signals: dictionnary with numeric signals as keys and routines as values

		Returns:

		"""
		for sig in signals.keys():
			try: signal.signal(sig, signals[sig]);
			except: continue;
	signal_handler = staticmethod(signal_handler);

	def no_list_rec(to_format, formated):
		"""
		converts multidimensional containers into one-dimensional containers
		Args:
		  to_format: container to convert
		  formated: converted result

		Returns: converted result

		"""
		for elmt in to_format:
			if isinstance(elmt, collections.Iterable) and not isinstance(elmt, str):
				UnivRes.no_list_rec(elmt,formated);
			else: formated.append(elmt);
		return formated;
	no_list_rec = staticmethod(no_list_rec);
